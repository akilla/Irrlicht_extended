// Copyright (C) 2017 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h
#pragma once
#ifndef __IRR_I_BGFX_MANIPULATOR_H_INCLUDED__
#define __IRR_I_BGFX_MANIPULATOR_H_INCLUDED__

#include "IReferenceCounted.h"
#include "IBgfxBuffer.h"
#include "IComputeShader.h"

namespace irr
{
	namespace video
	{
	
	enum E_BGFX_DEBUG_FLAGS
	{
		E_NONE = 0,
		E_WIREFRAME = 1,
		E_IFH_TEST = 2, //Infinitely fast hardware test - no rendering
		E_STATS = 4, //show fps, memory etc.
		E_TEXT = 8
	};


	class IBgfxManipulator : public virtual IReferenceCounted
	{
		public:
			virtual ~IBgfxManipulator(){};
		
			/**
			* @brief Set the Debug Mode of the BgfxDriver
			* @param debugFlag pass one of the E_BGFX_DEBUG_FLAGS
			*/
			virtual void setBgfxDebugMode(unsigned int debugFlag){};

			///@brief set the BgfxBuffer to set the vertex/index buffer
			///@param buffer pointer to the buffer containing vertex and/or index buffer handles
			virtual void setBgfxBuffer(IBgfxBuffer* buffer) {};
			
			///@brief set a ComputeBuffer 
			///@param buffer pointer to the buffer containing vertex and/or index compute buffer handles
			virtual void setComputeBuffer(unsigned char stage, IBgfxBuffer* buffer, E_BGFX_COMPUTE_ACCESS_FLAGS flag) {};

			///@brief set a Buffer used as an Instancebuffer
			/// only dynamic vertex or static vertex buffers supported
			///@param buffer vertexbuffer used as instancebuffer
			///@param startVertex first vertex in bufferarray
			///@param num number of vertices from startVertex
			virtual void setInstanceBuffer(IBgfxBuffer* buffer, unsigned int startVertex, unsigned int numOfVertices) {};

			///@brief render the set buffers with set Transforms, Materials and ActiveView
			virtual void renderCall() {};

			///@brief creates a BgfxComputeShader if you are using the BgfxDriver
			///Note: first addUniform and afterwards call createProgram on the ComputeShader
			///or else bgfx won't set the uniforms
			///@return pointer to the ComputeShader if success else nullptr
			virtual IComputeShader* createBgfxComputeShader() {return NULL; };

			///@brief creates a BgfxBuffer if you are using the BgfxDriver
			///@param dynamic is the Buffer dynamic?
			///@return pointer to the BgfxBuffer if success else nullptr
			virtual IBgfxBuffer* createBgfxBuffer(bool dynamic) { return NULL;}
	};
	
}//namespace video

}//namespace irr

#endif //BGFX_MANIPULATOR
