// Copyright (C) 2017 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h
#pragma once
#ifndef __IRR_I_COMPUTE_SHADER_H_INCLUDED__
#define __IRR_I_COMPUTE_SHADER_H_INCLUDED__

#include <stdint.h>
#include "IReferenceCounted.h"

namespace irr
{
namespace video
{

	enum E_BGFX_COMPUTE_ACCESS_FLAGS
	{
		READ,
		WRITE,
		READWRITE,
		E_BGFX_COMPUTE_FLAG_COUNT
	};


	class IComputeShader : public virtual IReferenceCounted
	{
	public:
		virtual ~IComputeShader() {};


		virtual void createShaderProgram(const uint8_t * computeShaderCode, const uint32_t csSize) = 0;

		///@brief add a BgfxBuffer to the computeshaderpipeline
		///@param stage set the position (0,1,2..) of the buffer in your shader 
		///@param buffer pointer to the buffer
		///@param accessFlag  are you performing write, read or readwrite operations on the buffer?
		///Note: if the buffer contains vertex and index buffer vertexBuffer will be at stage and indexBuffer at stage+1
		virtual void addBgfxBuffer(uint8_t stage, IBgfxBuffer* buffer, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag) = 0;
		
		
		//right now only use the vec4f uniformtype of bgfx
		//TODO: add uniformtypes to irrlicht
		///@brief creates a Uniform of type vec4
		///To set the uniform just call setUniform once later
		///@param name of the uniform in the shader
		///@param numOfElements  pass number of vec4
		virtual void addUniform(const char* name, uint16_t numOfElements) = 0;
		///@brief creates a Uniform of type vec4 and directly sets the value
		///Note: no need to call setUniform after this call
		///@param name of the uniform in the shader
		///@param value pointer to the data
		///@param numOfElements  pass number of vec4
		virtual void addUniform(const char* name, void* const value, uint16_t numOfElements) = 0;
		///@brief set the Uniform data
		///Note: make sure you created a Uniform of that name before calling this function
		///@param name name of the Uniform
		///@param value pointer to the data
		virtual void setUniform(const char* name, void* const value) = 0;

		///@brief reset the internally used Cache of Uniforms
		///all added Uniform will be forgotten by the ComputeShader
		virtual void resetUniformCache() = 0;

		///@brief reset the internally used Cache of Buffers
		///all added Buffers will be forgotten by the ComputeShader
		virtual void resetBufferCache() = 0;


		///@brief runs the computeshader
		///set the view and the dimension of processing : https://www.khronos.org/opengl/wiki/Compute_Shader
		///@param view - view number: 0-255
		///@param groupsX - numberofgroups X
		///@param groupsY - numberofgroups Y
		///@param groupsZ - numberofgroups Z
		virtual void run(uint8_t view, uint16_t groupsX, uint16_t groupsY, uint16_t groupsZ) = 0;

		///@create a new ComputeShaderInstance with the same ShaderProgram
		///but without the set Buffers/Uniforms
		///@return Pointer to a new ComputeShaderObject
		virtual IComputeShader* getNewInstance() = 0;

		
	};

}//video
}//irr
#endif //COMPUTE_SHADER
