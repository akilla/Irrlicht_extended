// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in Irrlicht.h

#include "CBgfxCacheHandler.h"

#ifdef _IRR_COMPILE_WITH_BGFX_

#include "CBgfxDriver.h"

namespace irr
{
namespace video
{

/* CBgfxCacheHandler */

CBgfxCacheHandler::CBgfxCacheHandler(CBgfxDriver* driver) :
	CBgfxCoreCacheHandler<CBgfxDriver, CBgfxTexture>(driver), AlphaMode(false), AlphaRef(0.f), AlphaTest(false),
	ClientStateVertex(false), ClientStateNormal(false), ClientStateColor(false), ClientStateTexCoord0(false)
{

}

CBgfxCacheHandler::~CBgfxCacheHandler()
{
}

void CBgfxCacheHandler::setAlphaFunc(bool mode, f32 ref)
{
	if (AlphaMode != mode || AlphaRef != ref)
	{
		AlphaMode = mode;
		AlphaRef = ref;
	}
}

void CBgfxCacheHandler::setAlphaTest(bool enable)
{
	AlphaTest = enable;
}


//void CBgfxCacheHandler::setMatrixMode(GLenum mode)
//{
//	if (MatrixMode != mode)
//	{
//		glMatrixMode(mode);
//		MatrixMode = mode;
//	}
//}

//void CBgfxCacheHandler::setClientActiveTexture(GLenum texture)
//{
//	if (ClientActiveTexture != texture)
//	{
//		Driver->irrGlClientActiveTexture(texture);
//		ClientActiveTexture = texture;
//	}
//}

uint64_t CBgfxCacheHandler::getBgfxStateFlag(u32 viewIndx) const
{
	if(FrameBufferCount <= viewIndx)
		return 0;

	return 0| (DepthMask ? BGFX_STATE_DEPTH_WRITE : 0) | (ColorMask[viewIndx][3]? BGFX_STATE_ALPHA_WRITE : 0)
			| (ColorMask[viewIndx][0]? BGFX_STATE_RGB_WRITE : 0) | (Blend[viewIndx] ? BlendEquation[viewIndx] : 0)
			| (CullFace[viewIndx] ? CullFaceMode[viewIndx] : 0) | (DepthTest ? DepthFunc : 0) | (AlphaMode ? BGFX_STATE_ALPHA_REF(AlphaRef) : 0 ) | extraFlag;
}

void CBgfxCacheHandler::setActiveView(u8 view)
{
	//std::cerr << "CBgfxCacheHandler::setActiveView: active view(" << static_cast<int>(activeView) << ")->" << static_cast<int>(view) << ", prev(" << static_cast<int>(prevView) << ")" << std::endl;
	if(view != activeView)
	{
		prevView = activeView;
		activeView = view;
	}
}

void CBgfxCacheHandler::setActiveViewToPrevious()
{
	//std::cerr << "CBgfxCacheHandler::setActiveViewToPrevious: active view(" << static_cast<int>(activeView) << ") -> prev(" << static_cast<int>(prevView) << ")" << std::endl;
	activeView = prevView;
}

void CBgfxCacheHandler::updateQueuedObjects(u32 frame)
{
	if(queuedLockUpdates.empty())
	{
		return;
	}

	for(auto it = queuedLockUpdates.begin(); it != queuedLockUpdates.end(); ++it)
	{
		if((*it)->updateLockData(frame))
		{
			it = queuedLockUpdates.erase(it);
		}
	}

}

void CBgfxCacheHandler::addLockTextureToQueue(CBgfxCoreTexture<CBgfxDriver>* texture)
{
	if(texture)
		queuedLockUpdates.push_back(texture);
}

} // end namespace
} // end namespace

#endif //#ifdef _IRR_COMPILE_WITH_BGFX_
