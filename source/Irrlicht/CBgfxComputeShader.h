// Copyright (C) 2017 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h
#pragma once
#include "IrrCompileConfig.h"

#ifdef _IRR_COMPILE_WITH_BGFX_

#include <vector>
#include <typeinfo>
#include <string>
#include "IMaterialRendererServices.h"
#include "CNullDriver.h"

#include <bgfx/bgfx.h>
#include "CBgfxAndIrrlichtTypes.h"
#include "IComputeShader.h"

namespace irr
{
namespace video
{
	typedef uint16_t bgfx_handle_t;

	class CBgfxComputeShader : public IComputeShader
	{
	public:
		CBgfxComputeShader();
		virtual ~CBgfxComputeShader();

		virtual void createShaderProgram(const uint8_t* computeShaderCode, const uint32_t csSize);

		virtual void addBgfxBuffer(uint8_t stage,IBgfxBuffer* buffer, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag);

		///@brief add a DynamicIndexBuffer to the computeshaderpipeline
		///@param stage set the position (0,1,2..) of the buffer in your shader
		///@param handle of the buffer
		///@param accessFlag  are you performing write, read or readwrite operations on the buffer?
		virtual void addBuffer(uint8_t stage, bgfx::DynamicIndexBufferHandle bufferHandle, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag);

		///@brief add a DynamicVertexBuffer to the computeshaderpipeline
		///@param stage set the position (0,1,2..) of the buffer in your shader
		///@param handle of the buffer
		///@param accessFlag  are you performing write, read or readwrite operations on the buffer?
		virtual void addBuffer(uint8_t stage, bgfx::DynamicVertexBufferHandle bufferHandle, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag);

		///@brief add a IndexBuffer to the computeshaderpipeline
		///@param stage set the position (0,1,2..) of the buffer in your shader
		///@param handle of the buffer
		///@param accessFlag  are you performing write, read or readwrite operations on the buffer?
		virtual void addBuffer(uint8_t stage, bgfx::IndexBufferHandle bufferHandle, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag);

		///@brief add a VertexBuffer to the computeshaderpipeline
		///@param stage set the position (0,1,2..) of the buffer in your shader
		///@param handle of the buffer
		///@param accessFlag  are you performing write, read or readwrite operations on the buffer?
		virtual void addBuffer(uint8_t stage, bgfx::VertexBufferHandle bufferHandle, E_BGFX_COMPUTE_ACCESS_FLAGS accessFlag);

		//right now only use the vec4f uniformtype of bgfx
		//TODO: add uniformtypes to irrlicht
		virtual void addUniform(const char* name, void* const value, uint16_t numOfElements);

		virtual void addUniform(const char* name, uint16_t numOfElements);

		virtual void setUniform(const char* name,  void* const value);

		///@brief reset the internally used Cache of Uniforms
		///all added Uniform will be forgotten by the ComputeShader Uniform will be destroyed
		virtual void resetUniformCache();
		///@brief reset the internally used Cache of Buffers
		///all added Buffers will be forgotten by the ComputeShader
		virtual void resetBufferCache();

		///@brief runs the computeshader
		///set the view and the dimension of processing : https://www.khronos.org/opengl/wiki/Compute_Shader
		///@param view - view number: 0-255
		///@param groupsX - numberofgroups X
		///@param groupsY - numberofgroups Y
		///@param groupsZ - numberofgroups Z
		virtual void run(uint8_t view, uint16_t groupsX, uint16_t groupsY, uint16_t groupsZ);
		
		///@create a new CBgfxComputeShaderInstance with the same ShaderProgram
		///but without the set Buffers/Uniforms
		///@return Pointer to a new ComputeShaderObject
		CBgfxComputeShader* getNewInstance();

	private:

		CBgfxComputeShader(const CBgfxComputeShader& cS);

		bgfx::ProgramHandle computeShaderProgram = BGFX_INVALID_HANDLE;
		

		struct ComputeBuffer
		{
			enum E_BUFFERTYPE
			{
				DYNAMIC_INDEX,
				DYNAMIC_VERTEX,
				STATIC_INDEX,
				STATIC_VERTEX,
				COUNT
			} bufferType;
			//handle value
			bgfx_handle_t handle = bgfx::kInvalidHandle;

			E_BGFX_COMPUTE_ACCESS_FLAGS computeAccessFlag = E_BGFX_COMPUTE_ACCESS_FLAGS::E_BGFX_COMPUTE_FLAG_COUNT;

			ComputeBuffer()
			{}
			ComputeBuffer(E_BUFFERTYPE bufferType_, bgfx_handle_t handleidx, E_BGFX_COMPUTE_ACCESS_FLAGS accFlag):
				bufferType(bufferType_),handle(handleidx),computeAccessFlag(accFlag)
			{ }
		};

		struct UniformInformation
		{
			std::string uniformName;
			bgfx::UniformHandle uniformHandle = BGFX_INVALID_HANDLE;
			void* data = nullptr;

			UniformInformation()
			{};
			UniformInformation(std::string name, bgfx::UniformHandle handle, void* data_) :
				uniformName(name), uniformHandle(handle), data(data_)
			{};
		};

		std::vector<ComputeBuffer> buffers;
		std::vector<UniformInformation> uniforms;

		bgfx::Access::Enum convertIrrAccessFlagToBgfx(E_BGFX_COMPUTE_ACCESS_FLAGS flag)
		{
			switch (flag)
			{
			case irr::video::READ:
				return bgfx::Access::Read;
				break;
			case irr::video::WRITE:
				return bgfx::Access::Write;
				break;
			case irr::video::READWRITE:
				return bgfx::Access::ReadWrite;
				break;
			case irr::video::COUNT:
			default:
				return bgfx::Access::Count;
				break;
			}
		}
	};
	

}//namespace video
}//namespace irr
#endif
