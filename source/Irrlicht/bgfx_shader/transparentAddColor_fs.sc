$input v_color0, v_texcoord0, v_normal

#include "common.sh"

SAMPLER2D(texture0,  0);

void main()
{		
	vec4 texel = texture2D(texture0, v_texcoord0);
	vec4 color = vec4(texel.rgb,1.0*max(max(texel.r,texel.g),texel.b));
	gl_FragColor = color;
}
