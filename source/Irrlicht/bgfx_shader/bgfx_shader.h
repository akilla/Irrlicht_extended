///:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
///GLSL
///:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//Basic Shader - no texcoords
#include "basic_vs_glsl_bgfx.h"
#include "basic_fs_glsl_bgfx.h"

#include "standard_vs_glsl_bgfx.h"
#include "standard_fs_glsl_bgfx.h"

#include "onetextureblend_vs_glsl_bgfx.h"
#include "onetextureblend_fs_glsl_bgfx.h"

#include "transparentAddColor_vs_glsl_bgfx.h"
#include "transparentAddColor_fs_glsl_bgfx.h"

///:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
///HLSL
///:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#include "basic_vs_hlsl.h"
#include "basic_fs_hlsl.h"

#include "standard_vs_hlsl.h"
#include "standard_fs_hlsl.h"

#include "onetextureblend_vs_hlsl.h"
#include "onetextureblend_fs_hlsl.h"

#include "transparentAddColor_vs_hlsl_bgfx.h"
#include "transparentAddColor_fs_hlsl_bgfx.h"
