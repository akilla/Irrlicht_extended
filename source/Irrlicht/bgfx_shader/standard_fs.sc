$input v_color0, v_texcoord0, v_normal

#include "common.sh"

SAMPLER2D(texture0,  0);

void main()
{		
		gl_FragColor = texture2D(texture0, v_texcoord0);
		
}
