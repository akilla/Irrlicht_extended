// Copyright (C) 2017 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#pragma once

#include "IrrCompileConfig.h"

#ifdef _IRR_COMPILE_WITH_BGFX_

#include <bgfx/bgfx.h>
#include "IBgfxBuffer.h"


namespace irr
{
namespace video
{

	class CBgfxBuffer : public IBgfxBuffer
	{
	public:
		CBgfxBuffer(bool dynamic_ = false);
		virtual ~CBgfxBuffer();

		virtual void addVertexAttribute(E_BGFX_VERTEX_ATTRIBUTE attrib, uint8_t numberOfElements, E_BGFX_ATTRIBUTE_TYPE attribType, bool normalize = false, bool asInt = false);

		virtual void processVertexAttributes();

		virtual void setVertexBuffer(const void* buffer, size_t vertexCount, size_t vertexSize, bool computeBuffer);

		virtual void setVertexBuffer(const size_t bufferSize, bool computeBuffer);

		virtual void setIndexBuffer(const void* buffer, size_t indexCount, E_INDEX_TYPE idxType, bool computeBuffer);
		
		virtual void setIndexBuffer(const size_t bufferSize, E_INDEX_TYPE idxType, bool computeBuffer);

		bool isDynamic() { return dynamic; }


		virtual void destroyVertexBuffer();

		virtual void destroyIndexBuffer();

		virtual void destroyBuffers();

		bgfx::DynamicVertexBufferHandle getDynamicVertexBuffer();

		bgfx::DynamicIndexBufferHandle getDynamicIndexBuffer();

		bgfx::VertexBufferHandle getVertexBuffer();

		bgfx::IndexBufferHandle getIndexBuffer();
	
	private:
		bool dynamic;
		bgfx::VertexDecl vDecl;
		bool firstAttribute = true;

		BGFX_HANDLE(TemporaryVHandle)
		BGFX_HANDLE(TemporaryIHandle)
		TemporaryVHandle vHandle = BGFX_INVALID_HANDLE;
		TemporaryIHandle iHandle = BGFX_INVALID_HANDLE;


	};

}//video
}//irr

#endif //compile with bgfx
