// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in Irrlicht.h

#ifndef __C_OPENGL_CACHE_HANDLER_H_INCLUDED__
#define __C_OPENGL_CACHE_HANDLER_H_INCLUDED__

#include "IrrCompileConfig.h"

#ifdef _IRR_COMPILE_WITH_BGFX_
#include "CBgfxCommon.h"

#include "CBgfxCoreFeature.h"
#include "CBgfxCoreTexture.h"
#include "CBgfxCoreCacheHandler.h"


namespace irr
{
namespace video
{

	class CBgfxCacheHandler : public CBgfxCoreCacheHandler<CBgfxDriver, CBgfxTexture>
	{
	public:
		CBgfxCacheHandler(CBgfxDriver* driver);
		virtual ~CBgfxCacheHandler();

		// Alpha calls.

		void setAlphaFunc(bool mode, f32 ref);

		void setAlphaTest(bool enable);


		// Client state calls.

		//void setClientState(bool vertex, bool normal, bool color, bool texCoord0);

		// Matrix calls.

		//void setMatrixMode(GLenum mode);

		// Texture calls.

		//void setClientActiveTexture(GLenum texture);

		uint64_t getBgfxStateFlag(u32 viewIndx) const _IRR_OVERRIDE_;

		//bool setUniformValue(const char* name,const void* value);

		inline uint8_t getActiveView() const
		{
			return activeView;
		}

		inline uint8_t getPreviousView() const
		{
			return prevView;
		}

		inline uint8_t get2DView() const
		{
			return GuiView;
		}

		void setActiveView(uint8_t view);

		void setActiveViewToPrevious();

		void updateQueuedObjects(u32 frame);

		void addLockTextureToQueue(CBgfxCoreTexture<CBgfxDriver>* texture);

	protected:
		bool AlphaMode;
		f32 AlphaRef;
		bool AlphaTest;

		//u32 MatrixMode;

		//u32 ClientActiveTexture;

		bool ClientStateVertex;
		bool ClientStateNormal;
		bool ClientStateColor;
		bool ClientStateTexCoord0;

		core::array<bgfx::UniformHandle> UniformHandleArray;

		CBgfxTextureList queuedLockUpdates;

	};

} // end namespace video
} // end namespace irr

#endif //#ifdef _IRR_COMPILE_WITH_BGFX_

#endif
