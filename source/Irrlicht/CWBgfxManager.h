// Copyright (C) 2016 Julius Tilly
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in Irrlicht.h

#ifndef __C_WBGFX_MANAGER_H_INCLUDED__
#define __C_WBGFX_MANAGER_H_INCLUDED__

#include "IrrCompileConfig.h"

#ifdef _IRR_COMPILE_WITH_WBGFX_MANAGER_

#include "SIrrCreationParameters.h"
#include "SExposedVideoData.h"
#include "IContextManager.h"
#include "SColor.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>

#include <bgfx/bgfx.h>
#include <bgfx/platform.h>

namespace irr
{
namespace video
{
    // WBgfx manager.
    class CWBgfxManager : public IContextManager
    {
    public:
        //! Constructor.
        CWBgfxManager();

		//! Destructor
		~CWBgfxManager();

        // Initialize
        bool initialize(const SIrrlichtCreationParameters& params, const SExposedVideoData& data);

        // Terminate
        void terminate();

        // Create surface.
        bool generateSurface();

        // Destroy surface.
        void destroySurface();

        // Create context.
        bool generateContext();

        // Destroy EBgfx context.
        void destroyContext();

		//! Get current context
		const SExposedVideoData& getContext() const;

		//! Change render context, disable old and activate new defined by videoData
		bool activateContext(const SExposedVideoData& videoData);

        // Swap buffers.
        bool swapBuffers();

    private:
        SIrrlichtCreationParameters Params;
		SExposedVideoData PrimaryContext;
        SExposedVideoData CurrentContext;
		s32 PixelFormat;
		PIXELFORMATDESCRIPTOR pfd;
		ECOLOR_FORMAT ColorFormat;
		bgfx::PlatformData bgfxPlatformData;
	};
}
}

#endif //#ifdef _IRR_COMPILE_WITH_BGFX_

#endif
